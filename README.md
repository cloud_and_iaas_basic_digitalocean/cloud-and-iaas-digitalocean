</details>

******

<details>
<summary>Step 0: Clone Git Repository </summary>
 <br />

**steps:**

```sh
# create local directory and cd into it
mkdir node-project
cd node-project

# clone repository
git clone git@gitlab.com:twn-devops-bootcamp/latest/05-cloud/cloud-basics-exercises.git

# remove remote repo reference and create your own local repository
rm -rf .git
git init 
git add .
git commit -m "initial commit"

# create git repository on Gitlab and push your newly created local repository to it
git remote add origin git@gitlab.com:{gitlab-user}/{gitlab-repo}.git
git push --set-upstream origin main

```

</details>

******

<details>
<summary>Step 1: Package NodeJS App </summary>
 <br />

**steps**

```sh
cd app
npm pack

```

</details>

******

<details>
<summary>Steps 2, 3: Create and prepare server on Digital Ocean, lockdownn server using firewall open ports 22 (allow specific ip address) and 3000 </summary>
 <br />

**steps:**
```sh
# ssh into your newly created server
ssh root@{server-ip-address}

# install nodejs and npm
apt install -y nodejs npm

```

</details>

******

<details>
<summary>Steps 4, 5: Create Application user & add the user to sudo group </summary>
 <br />

**steps:**
```sh
# create new app user
adduser newuser

# add user to sudo group
usermod -aG sudo newuser

# verify user belongs to group
groups newuser

# verify sudo access
su - newuser
ls /home

```

</details>

******

<details>
<summary>Steps 6: Create .ssh directory and authorized_keys file within the Application user's directory</summary>
 <br />

**steps:**
```sh
# on you local machine, copy your public key
pbcopy < ~/.ssh/id_rsa.pub

# switch to your server tab on your terminal or ssh to your droplet, confirm user is application user
ssh newuser@{server-ip-address}:/home/newuser
whoami 

# create .ssh directory and authorized_keys file paste you id_rsa key to enable ssh connection to application user profile
mkdir .ssh
cd .ssh
vim authorized_keys (paste public key here)

```

</details>

******


<details>
<summary>Step 7: Copy App to the application user home directory (best practice)</summary>
 <br />

**steps:**
```sh
# secure copy files from local machine to server. Execute from project's root folder.
scp bootcamp-node-project-1.0.0.tgz newuser@{server-ip-address}:/home/newuser

```

</details>

******

<details>
<summary>Step 8: Run Node App </summary>
 <br />

**steps:**
```sh
# ssh into droplet
ssh newuser@{server-ip-address}

# unpack the node-project file
tar -xzf bootcamp-node-project-1.0.0.tgz

# change into unpacked directory called "package"
cd package

# install dependencies
npm install

# run the application in detached mode
npm start &

```

</details>

******

<details>
<summary>Step 9: Check application is running </summary>
 <br />

**steps:**
```sh
# check node application is running
ps aux | grep node

# check the port used
netstat -lnpt

# confirm application on browser
serveraddress:port 

# you can kill the process. Application will no longer be accessed (optional) 
kill -9 PROCESS_ID

```

</details>

******
